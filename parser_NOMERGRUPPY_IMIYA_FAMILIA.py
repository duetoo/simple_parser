import requests
from bs4 import BeautifulSoup
from collections import Counter


events = list()
magnitudes = list()
locations = list()

website_url = requests.get(r'https://en.wikipedia.org/wiki/List_of_20th-century_earthquakes').text
soup = BeautifulSoup(website_url, "html.parser")
my_table = soup.find_all('table',{'class':'sortable wikitable'})
rows = my_table[0].find_all('tr')
events.append(rows[0].find_all('th')[3].get_text().strip())
locations.append(rows[0].find_all('th')[4].get_text().strip())
magnitudes.append(rows[0].find_all('th')[2].get_text().strip())

for row in rows[1:]:
    r = row.find_all('td')
    events.append(r[3].get_text().strip())
    magnitudes.append(r[2].get_text().strip())
    locations.append(r[4].get_text().strip())
print(events, magnitudes, locations, sep='\n\n')

for i in range(1, len(magnitudes)):
    magnitudes[i] = magnitudes[i].replace("Ms", "")
    magnitudes[i] = float(magnitudes[i])
print(magnitudes)

earthquakes = {k: list(v) for (k, v) in zip(events[1:], zip(locations[1:], magnitudes[1:]))}
print(earthquakes)


def info_print(event_name:str):
    print(f"{event_name} happend in {earthquakes[event_name][0]} with magnitude of {earthquakes[event_name][1]}")


info_print('1970 Ancash earthquake')

count_earthquakes = Counter(locations[1:])
for key, value in count_earthquakes.items():
    print(f"{key} - {value}")

